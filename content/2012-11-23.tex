% Henri Menke, Michael Schmid, 2012 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

% Vorlesung am 23.11.2012

\subsection{Überlagerung (Interferenz) von Materiewellen}

Ein klassisches, optisches Experiment stellt der \acct[0]{Poisson'sche Fleck}\index{Poisson'scher Fleck} dar. Der Versuch ist wie folgt aufgebaut:
%
\begin{figure}[H]
  \centering
  \begin{pspicture}(0,-4)(8,1)
    \pscircle(0,0){0.2}
    \psline(0.2;-135)(0.2;45)
    \psline(0.2;-45)(0.2;135)
    \psline(0.3,0)(3.9,0)
    \psline(4,-1)(4,1)
    \psline(4.1,0)(7.9,0)
    \psline(8,-1)(8,1)
    \psline{<->}(4.1,0.5)(7.9,0.5)
    \rput*(6,0.5){\color{DimGray} mehrere Meter}
    \rput(6,-0.5){\color{DimGray} $a$}
    \uput[-90](4,-1){\color{DimGray} Hindernis}
    \uput[-90](0,-0.2){\parbox{8em}{\color{DimGray}\centering Lichtquelle \\ Laser + Objektiv zum Aufweiten}}
    \uput[0](8,0){\parbox{7em}{\color{DimGray}\centering Schirm bzw. CCD Chip zur Detektion}}
    
    \psline[linecolor=DarkOrange3]{->}(4,-1.5)(3.5,-2)
    \psline[linecolor=MidnightBlue]{->}(4,-1.5)(4.5,-2)
    
    \rput(6,-2){
      \pscircle*[linecolor=MidnightBlue](0,0){0.2}
      \uput[-90](0,-0.2){\color{MidnightBlue} Schraubenkopf}
    }
    \rput(2,-3){
      \pscustom[fillstyle=vlines,hatchcolor=DarkOrange3,linecolor=DarkOrange3]{\psline[linecolor=DarkOrange3](0,0)(0,1)(0.5,1.5)(0.5,0.5)(0,0)}
      \psellipse[fillstyle=solid,linecolor=DarkOrange3](0.25,0.75)(0.1,0.25)
      \uput[-90](0.25,0){\color{DarkOrange3} Blende $\varnothing 5 \; \mathrm{mm}$}
    }
  \end{pspicture}
\end{figure}

\begin{figure}[H]
  Mit Blende:
  \begin{center}
    \begin{pspicture}(-0.5,-0.5)(0.5,0.5)
      \pscircle[linewidth=3pt](0,0){0.1}
      \pscircle[linewidth=2.5pt](0,0){0.2}
      \pscircle[linewidth=2pt](0,0){0.3}
      \pscircle[linewidth=1.5pt](0,0){0.4}
      \pscircle[linewidth=1pt](0,0){0.5}
      
      \rput{0}(0,0){
        \pscurve[linecolor=DarkOrange3](0.0,0)(0.7,0.1)(0.7,-0.1)(1.4,0)
        \uput[0]{0}(1.4,0){\color{DarkOrange3} Dunkler Poissonscher Fleck}
      }
    \end{pspicture}
  \end{center}
  Der dunkle poissonsche Fleck lässt sich von dunkel nach hell ändern, je nach Blendendurchmesser bzw. Abstand von Hindernis und Schirm.
\end{figure}

\begin{figure}[H]
  Mit Schraubenkopf:
  \begin{center}
    \begin{pspicture}(-0.7,-0.7)(0.7,0.7)
      \pscircle*(0,0){0.5}
      \pscircle(0,0){0.6}
      \pscircle(0,0){0.7}
      \pscircle[fillstyle=solid](0,0){0.1}
      \pscircle[linecolor=gray,linewidth=0.5pt](0,0){0.2}
      \pscircle[linecolor=gray,linewidth=0.5pt](0,0){0.3}
      \pscircle[linecolor=gray,linewidth=0.5pt](0,0){0.4}
      
      \rput{0}(0,0){
        \pscurve[linecolor=DarkOrange3](0.0,0)(0.7,0.1)(0.7,-0.1)(1.4,0)
        \uput[0]{0}(1.4,0){\color{DarkOrange3} Heller Poissonscher Fleck}
      }
      
      \rput{45}(0.2,0.2){
        \pscurve[linecolor=DarkOrange3](0.0,0)(0.7,-0.1)(0.7,0.1)(1.4,0)
        \uput[-45]{-45}(1.4,0){\color{DarkOrange3} Geometrische Schattenregion}
      }
      
      \rput{180}(-0.5,-0.5){
        \pscurve[linecolor=DarkOrange3](0.0,0)(0.7,0.1)(0.7,-0.1)(1.4,0)
        \uput[0]{180}(1.4,0){\color{DarkOrange3} Fresnelsche Beugungsringe}
      }
    \end{pspicture}
  \end{center}
\end{figure}

\begin{notice*}
  Der Poissonsche Fleck und die Fresnel'schen Beugungsringe sind eine direkte Manifestation der Wellennatur des Lichts.
\end{notice*}

Das Experiment von T. Pfau und J. Mlyneck ('97 Konstanz) zum Nachweis der Wellennatur von Heliumatomen ist wie folgt aufgebaut:

\begin{figure}[H]
  \begin{center}
    \begin{pspicture}(0,-1)(6,1)
      \psdot*(0,0)
      \uput[-90](0,0){\color{DimGray} He}
      \uput[90](0.2,0){\color{DimGray} $v = 3 \mathrm{\frac{km}{s}}$}
      \psline{->}(0.1,0)(0.9,0)
      \psline(1,-1)(1,-0.1)
      \psline(1,1)(1,0.1)
      \psline(5,-1)(5,1)
      \psline(1,0)(5,0.2)
      \psline(1,0)(5,-0.2)
      \pscircle*(3,0){0.1}
      \uput[90](3,0){\color{DimGray} Draht $4$ $\mu$m}
      \psline{<->}(1,-0.5)(3,-0.5)
      \psline{<->}(3,-0.5)(5,-0.5)
      \rput*(2,-0.5){\color{DimGray} $1.3$ m}
      \rput*(4,-0.5){\color{DimGray} $1.3$ m}
      \uput[-90](1,-1){\color{DimGray} $2$ $\mu$m Spalt}
      \uput[-90](5,-1){\color{DimGray} Schirm}
    \end{pspicture}
  \end{center}
\end{figure}

\begin{notice*}
  Der Poissonfleck ist die Manifestation der konstruktiven Interferenz der Materiewellen.
\end{notice*}

Die de Broglie-Wellenlänge ist einstellbar (durch die Geschwindigkeit $v$):
\[
  \lambda_{dB} = \frac{h}{p} = \frac{h}{mv}
\]

\begin{figure}[H]
  \begin{center}
    \begin{pspicture}(-0.2,-0.2)(3.5,2)
      \psaxes[labels=none,ticks=none]{->}(0,0)(-0.2,-0.2)(3.5,2)[{\color{DimGray} He, $\lambda_{\text{dB}}$ (pm)},0][{\color{DimGray} Beugungsmusterposition ($\mu$m)},0]
      
      \psxTick[labelFontSize=\color{DimGray}](1){20}
      \psxTick[labelFontSize=\color{DimGray}](2){40}
      \psxTick[labelFontSize=\color{DimGray}](3){60}
      \psyTick[labelFontSize=\color{DimGray}](0.5){50}
      \psyTick[labelFontSize=\color{DimGray}](1.0){100}
      \psyTick[labelFontSize=\color{DimGray}](1.5){150}
      
      \psdots*[dotscale=0.3,linecolor=DarkOrange3](1.07,1.27)(1.07,1.34)(1.13,1.31)(1.08,1.31)(1.06,1.35)(1.04,1.35)(1.11,1.33)(1.17,1.23)(1.19,1.33)(1.15,1.22)(1.25,1.28)(1.24,1.35)(1.27,1.37)(1.29,1.22)(1.36,1.29)(1.31,1.20)(1.23,1.32)(1.29,1.35)(1.27,1.30)(1.38,1.32)(1.35,1.31)(1.49,1.35)(1.38,1.23)(1.55,1.33)(1.45,1.26)(1.42,1.24)(1.55,1.32)(1.63,1.27)(1.63,1.24)(1.50,1.23)(1.66,1.34)(1.60,1.27)(1.74,1.33)(1.74,1.27)(1.70,1.42)(1.72,1.35)(1.68,1.35)(1.84,1.36)(1.80,1.33)(1.80,1.31)(1.76,1.36)(1.91,1.32)(1.82,1.34)(1.93,1.29)(1.89,1.36)(1.90,1.39)(1.86,1.28)(1.88,1.45)(1.90,1.42)(2.00,1.28)(2.08,1.39)(2.10,1.35)(2.11,1.47)(2.07,1.44)(2.16,1.30)(2.14,1.46)(2.06,1.37)(2.11,1.34)(2.10,1.46)(2.13,1.32)(2.23,1.36)(2.24,1.32)(2.35,1.46)(2.19,1.32)(2.24,1.37)(2.22,1.37)(2.42,1.48)(2.38,1.43)(2.32,1.51)(2.31,1.38)(2.40,1.49)(2.46,1.43)(2.40,1.48)(2.43,1.42)(2.58,1.50)(2.49,1.50)(2.48,1.52)(2.52,1.40)(2.50,1.48)(2.58,1.50)(2.63,1.49)(2.66,1.41)(2.73,1.51)(2.59,1.43)(2.73,1.49)(2.66,1.49)(2.65,1.54)(2.77,1.50)(2.87,1.41)(2.73,1.39)(2.80,1.51)(2.78,1.38)(2.88,1.40)(2.82,1.51)(2.97,1.40)(2.93,1.55)(2.93,1.44)(3.03,1.48)(3.05,1.57)(3.03,1.49)
      
      \psdots*[dotscale=0.3,linecolor=DarkOrange3](0.97,0.96)(0.98,1.06)(0.99,0.99)(0.97,0.91)(1.03,1.03)(1.17,1.00)(1.12,0.91)(1.07,0.98)(1.26,1.08)(1.21,1.10)(1.15,0.92)(1.28,0.95)(1.32,0.98)(1.36,0.91)(1.35,0.99)(1.27,0.96)(1.27,1.09)(1.43,0.93)(1.39,1.04)(1.35,0.99)(1.35,1.03)(1.40,1.06)(1.45,0.96)(1.43,0.93)(1.50,0.96)(1.59,0.94)(1.59,1.03)(1.48,0.94)(1.51,1.05)(1.55,1.06)(1.59,1.06)(1.67,1.04)(1.73,0.97)(1.70,0.90)(1.70,1.04)(1.65,0.97)(1.72,0.96)(1.84,1.05)(1.78,0.91)(1.74,0.94)(1.83,0.90)(1.91,1.05)(1.93,0.98)(1.88,0.91)(1.94,0.93)(1.96,0.99)(1.84,1.08)(2.01,1.08)(1.89,0.91)(1.89,0.98)(1.93,0.95)(2.06,1.05)(1.96,0.93)(2.14,1.00)(2.11,0.97)(2.19,1.10)(2.23,1.02)(2.07,0.95)(2.10,1.01)(2.11,1.08)(2.14,1.05)(2.20,0.99)(2.35,0.92)(2.21,1.03)(2.32,1.06)(2.24,0.94)(2.33,1.00)(2.35,0.97)(2.33,0.98)(2.37,1.09)(2.33,1.02)(2.52,1.04)(2.43,0.97)(2.54,1.04)(2.54,1.10)(2.51,0.94)(2.56,1.08)(2.47,0.92)(2.48,1.05)(2.64,1.06)(2.68,1.09)(2.57,0.91)(2.60,0.99)(2.58,0.96)(2.72,1.01)(2.68,1.00)(2.78,0.97)(2.81,1.09)(2.81,1.09)(2.71,0.91)(2.74,1.01)(2.82,0.91)(2.89,0.91)(2.82,1.07)(2.86,0.93)(2.87,0.95)(2.95,0.90)(2.97,0.92)(3.06,1.06)(3.07,0.93)
      
      \psdots*[dotscale=0.3,linecolor=DarkOrange3](1.10,0.76)(0.95,0.70)(1.09,0.67)(1.05,0.70)(1.06,0.71)(1.08,0.65)(1.12,0.68)(1.11,0.81)(1.15,0.64)(1.14,0.66)(1.13,0.64)(1.27,0.64)(1.21,0.63)(1.29,0.73)(1.24,0.66)(1.30,0.71)(1.24,0.79)(1.34,0.69)(1.26,0.75)(1.42,0.73)(1.39,0.62)(1.49,0.77)(1.43,0.78)(1.40,0.64)(1.53,0.70)(1.58,0.62)(1.60,0.65)(1.53,0.64)(1.47,0.73)(1.55,0.69)(1.69,0.60)(1.72,0.61)(1.66,0.69)(1.66,0.71)(1.59,0.71)(1.71,0.61)(1.79,0.65)(1.68,0.56)(1.80,0.59)(1.75,0.63)(1.79,0.66)(1.91,0.65)(1.82,0.58)(1.86,0.60)(1.79,0.55)(1.88,0.58)(1.97,0.72)(1.96,0.62)(1.89,0.66)(2.08,0.57)(2.09,0.69)(2.07,0.64)(2.00,0.69)(2.10,0.65)(2.09,0.60)(2.20,0.63)(2.13,0.60)(2.23,0.56)(2.08,0.59)(2.11,0.56)(2.23,0.59)(2.23,0.52)(2.30,0.63)(2.37,0.64)(2.33,0.52)(2.40,0.58)(2.40,0.61)(2.26,0.49)(2.39,0.59)(2.44,0.61)(2.37,0.66)(2.48,0.66)(2.54,0.56)(2.50,0.58)(2.46,0.50)(2.50,0.58)(2.49,0.52)(2.58,0.63)(2.66,0.62)(2.57,0.55)(2.67,0.54)(2.71,0.47)(2.57,0.60)(2.73,0.53)(2.67,0.55)(2.75,0.57)(2.84,0.60)(2.73,0.49)(2.76,0.60)(2.84,0.44)(2.90,0.50)(2.76,0.45)(2.91,0.42)(2.86,0.46)(2.97,0.52)(2.85,0.44)(3.04,0.41)(2.94,0.60)(2.89,0.52)(3.04,0.41)
    \end{pspicture}
  \end{center}
\end{figure}

\minisec{Evolution der Messung}

Das statistische Ensemble wird nach genügend langer Zeitdauer das Interferenzmuster herausbilden. Dasselbe funktioniert auch mit einzelnen Photonen (stark abgeschwächte Lichtquelle).

Dadurch manifestiert sich die Kopenhagener Interpretation der Quantenmechanik ($|\psi|^2 \widehat{=}$ Wahrscheinlichkeitsdichte)

\begin{notice*}[Hidden Parameters?]
Gedankenexperiment:

Auf den Hörsaalboden sind rote Streifen gemalt, alle Studenten betreten den Raum durch die beiden Türen. Sie stellen sich auf die Streifen. Eine CCD Kamera an der Decke mit Farbfilter sieht die roten Streifen nicht. Der Beobachter der Kameraaufnahme sieht nur das statistische Ensemble der Studenten sich entwickeln (als IF Muster).
\end{notice*} 
