% Henri Menke, Michael Schmid, 2012 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

% Vorlesung 23.10.2012

Für den Fall, dass Licht in $x$-Richtung linear polarisiert ist, $\bm{E}_0 = (E_x , 0 , 0)^T$, gilt nach Maxwell:
%
\begin{align*}
  \rot \bm{E} &= - \bm{\dot{B}} \\
  \frac{\partial}{\partial z} E_x &= -\dot{B}_y
\end{align*}
%
Einsetzen und nach $t$ integrieren
%
\begin{align}
  B_y &= - \int 2 E_x k \cos(kz) \sin(\omega t) \; \mathrm{d}t \notag \\
  \bm{B}_{\text{ges}} &= \begin{pmatrix} 0 \\ 2 E_x \frac{k}{\omega} \\ 0 \end{pmatrix} \cos(kz) \cos(\omega t)
\end{align}

\begin{figure}[H]
  \centering
  \begin{pspicture}(-0.5,-1)(4,1)
    \psaxes[labels=none,ticks=none]{->}(0,0)(-0.5,-1)(5,1)[\color{DimGray} $z$,-90][\color{DimGray} $x$,180]
    \psplot[plotpoints=200,linecolor=MidnightBlue]{0}{4.5}{0.5*sin(2*x)}
    \psplot[plotpoints=200,linecolor=DarkOrange3]{0}{4.5}{0.5*cos(2*x)}
    \psframe[linestyle=none,fillstyle=hlines,hatchcolor=DimGray,hatchsep=2pt](-0.2,-0.8)(0,0.8)
    \uput[180](-0.2,0.4){\color{DimGray} Metallspiegel}
    \uput[-90](3.9269,0){\color{DarkOrange3} $B_y$}
    \uput[45](4.2,0.3){\color{MidnightBlue} $E_x$}
  \end{pspicture}
\end{figure}

\subsection*{Interferometer}
\minisec{Young-Doppelspalt}

\begin{figure}[H]
  \centering
  \begin{pspicture}(0,-1)(4.5,1)
    \psdot(0,0)
    \psarc[](0,0){0.2}{-20}{20}
    \psarc[](0,0){0.4}{-25}{25}
    \psarc[](0,0){0.6}{-30}{30}
    \psarc[](0,0){0.8}{-35}{35}
    \psframe(1,0.2)(1.2,1)
    \psframe(1,0.1)(1.2,-0.1)
    \psframe(1,-0.2)(1.2,-1)
    \rput(1.2,0.15){
      \psarc[](0,0){0.2}{-20}{20}
      \psarc[](0,0){0.4}{-25}{25}
      \psarc[](0,0){0.6}{-30}{30}
      \psarc[](0,0){0.8}{-35}{35}
    }
    \rput(1.2,-0.15){
      \psarc[](0,0){0.2}{-20}{20}
      \psarc[](0,0){0.4}{-25}{25}
      \psarc[](0,0){0.6}{-30}{30}
      \psarc[](0,0){0.8}{-35}{35}
    }
    \psframe[linestyle=none,fillstyle=hlines,hatchcolor=DarkOrange3](3,-1)(3.2,1)
    \rput{90}(3,-1){
      \psaxes[labels=none,ticks=none]{->}(0,0)(0,0)(2.2,0.5) %[\color{DimGray} $x$,-90][\color{DimGray} $I(x)$,180]
      \uput[180]{-90}(0,0.5){\color{DimGray} $I(x)$}
      \uput[-90]{-90}(2.2,0){\color{DimGray} $x$}
      \psplot[plotpoints=200]{0}{2}{0.3*cos(10*x)^2}
      \uput[-90]{-90}(1,-0.1){\color{DimGray} $\propto \cos^2 x$}
    }
  \end{pspicture}
\end{figure}

Im folgendem Bild ist die Beugung von Wellen einer Punktlichtquelle am Einfachspalt zu sehen:
\begin{figure}[H]
  \centering
  \begin{pspicture}(0,-1)(4.5,1)
    \psdot(0,0)
    \psarc[](0,0){0.2}{-20}{20}
    \psarc[](0,0){0.4}{-25}{25}
    \psarc[](0,0){0.6}{-30}{30}
    \psarc[](0,0){0.8}{-35}{35}
    \psframe(1,0.1)(1.2,1)
    \psframe(1,-0.1)(1.2,-1)
    \psframe[linestyle=none,fillstyle=hlines,hatchcolor=DarkOrange3](3,-1)(3.2,1)
    \rput{90}(3,-1){
      \psaxes[labels=none,ticks=none]{->}(0,0)(0,0)(2.2,0.5) %[\color{DimGray} $x$,-90][\color{DimGray} $I(x)$,180]
      \uput[180]{-90}(0,0.5){\color{DimGray} $I(x)$}
      \uput[-90]{-90}(2.2,0){\color{DimGray} $x$}
      \rput(1,0){\psplot[plotpoints=200]{-1}{1}{0.005*(sin(10*x)/x)^2}}
      \uput[-90]{-90}(1,-0.1){\color{DimGray} $\propto \left(\frac{\sin x}{x}\right)^2$}
    }
  \end{pspicture}
\end{figure}

Experimentell wird folgendes Resultat für die Intensität erzielt:
\[
  I(x) \propto \left(\frac{\sin x}{x}\right)^2 = \mathrm{sinc}^2 x
\]
Die Periode des $\cos^2 x$ Beugungsbildes beim Doppelspalt hängt umgekehrt proportional vom Abstand der beiden Spalte ab, genauso hängt der Abstand der Minima beim Einfachspalt umgekehrt proportional mit der Spaltbreite zusammen. Im Fernfeld ist das Beugungsbild die Fouriertransformierte der Transmissionsfunktion des Spalts.

Komplexere Transmissionsfunktionen wie zum Beispiel breite Mehrfachspalte lassen sich bequem durch Faltungen von einfacheren Funktionen (zum Beispiel: rect-Funktion oder $\delta$-Paare) darstellen, Nach dem Faltungssatz (\acct{Parseval-Theorem}) ist das Beugungsbild dann einfach das Produkt der Fouriertransformierten. Wird das Fernfeld bei einem Abstand von $d \ll 10 \lambda$ beobachtet, es wird von der \emph{Fraunhofer-Region} gesprochen.

\minisec{Fresnel-Biprisma}

\begin{figure}[H]
  \centering
  \begin{pspicture}(0,-1)(4.5,1)
    \psline(0,0.5)(1,0.5)
    \psline(0,-0.5)(1,-0.5)
    \psline[linecolor=DarkOrange3](1,-0.7)(1,0.7)(1.5,0)(1,-0.7)
    \psline(1.15,0.5)(3,0)
    \psline(1.15,-0.5)(3,0)
    \psframe[linestyle=none,fillstyle=hlines,hatchcolor=DarkOrange3,hatchsep=2pt](3,-1)(3.2,1)
    \psline(3,-1)(3,1)
    \rput{90}(3,0){
      \psplot[linecolor=MidnightBlue]{-0.8}{0.8}{0.3*cos(10*x)^2}
      \uput[-90]{-90}(0,-0.1){\color{MidnightBlue} Interferenzmuster}
    }
  \end{pspicture}
\end{figure}

\minisec{Lloyd-Doppelspiegel}

\begin{figure}[H]
  \centering
  \begin{pspicture}(0,-0.5)(3.5,2)
    \psline(1,-0.5)(2,-0.2)
    \rput{16.699}(1.5,-0.35){
      \psframe[linestyle=none,fillstyle=hlines,hatchcolor=DimGray](-0.5,0)(0.5,-0.2)
    }
    \psline(2,-0.2)(3,0.5)
    \rput{34.992}(2.5,0.15){
      \psframe[linestyle=none,fillstyle=hlines,hatchcolor=DimGray](-0.5,0)(0.5,-0.2)
    }
    \psline(2.5,2)(3.5,1.5)
    \rput{-26.565}(3,1.75){
      \psframe[linestyle=none,fillstyle=hlines,hatchcolor=DimGray](-0.5,0)(0.5,0.2)
    }
    \rput{-26.565}(3,1.75){
      \psplot[linecolor=MidnightBlue,plotpoints=200]{-0.5}{0.5}{-0.005*(sin(10*x)/x)^2}
    }
    \psline[linecolor=DarkOrange3](0,0)(1.25,-0.4)(3,1.75)
    \psline[linecolor=DarkOrange3](0,0)(1.75,-0.25)(2.5,0.17)(3,1.75)
    \psdot(0,0)
  \end{pspicture}
\end{figure}

\minisec{Michelson-Interferometer}
Im Versuch wird ein beweglicher Spiegel um $d=20 \,\mu\mathrm{m}$ verschoben und $63$ (im Versuch $64$) Hell-Dunkel-Sequenzen im Interferenzmuster beobachtet. Der Versuchsaufbau ist im folgendem Bild zu sehen:

\begin{figure}[H]
  \centering
  \psset{unit=1.5cm}
  \begin{pspicture}(0,-1)(4.5,1)
    % Strahlen
    \psline{->}(0,0)(0.5,0)
    \psline(0,0)(2,0)
    \psline{<->}(1.3,0)(1.7,0)
    \psline(1,1)(1,-1)
    \psline{<->}(1,0.3)(1,0.7)
    \psline{->>}(1,0)(1,-0.5)
    \psline[linestyle=dotted,dotsep=1pt](2,0)(2.5,0)
    % Spiegel
    \psline[linecolor=DarkOrange3](2,-0.5)(2,0.5)
    \psline[linecolor=DarkOrange3](2.5,-0.5)(2.5,0.5)
    \psline[linecolor=DarkOrange3](0.5,1)(1.5,1)
    \uput[0](2.5,0){\color{DarkOrange3} verschiebbarer Spiegel}
    \rput{-45}(1,0){
      \psframe[framearc=10,fillstyle=solid](-0.05,-0.5)(0.05,0.5)
    }
    \psellipse*(1,-1)(0.1,0.05)
    \psellipse(1,-1)(0.2,0.1)
    \psellipse(1,-1)(0.3,0.15)
  \end{pspicture}
  \psset{unit=1cm}
\end{figure}

Wählt man:
%
\begin{align*}
  \Delta d &= m \frac{\lambda}{2}
\intertext{mit $m\in \mathbb{Z}$, erhält man eine Hell-Dunkel Sequenz am Detektor.}
  \frac{\lambda}{2} &= \frac{d}{\text{Anzahl}} = \frac{20 \, \mu\mathrm{m}}{64} = 0.3125 \,\mu\mathrm{m}\\
  \Rightarrow \lambda &= 625 \,\mathrm{nm} 
\end{align*}
%
Der verwendete Laser besitzt eine Wellenlänge von $\lambda = 632.8 \,\mathrm{nm}$

Die ausgedehnten Ringe kommen zustande, weil man mit leicht divergentem Licht arbeitet und sich die Wellenlängendifferenz als Vielfaches von ${\lambda}/{2}$ manifestieren.
