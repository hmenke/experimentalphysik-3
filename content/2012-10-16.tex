% Henri Menke, Michael Schmid, 2012 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

% Vorlesung vom 19.10.2012

\section{Elektromagnetische Wellen im Vakuum}

\subsection{Wellengleichung}

Die Maxwellschen Gleichungen sind Axiome der Elektrodynamik (klassische Betrachtung).

\begin{theorem*}[Maxwellgleichungen] in differentieller Form
  \begin{align}
    \rot \bm{E} &= - \frac{\partial \bm{B}}{\partial t} && \text{\color{DimGray} Induktionsgesetz} \\
    \div \bm{B} &= 0  && \text{\color{DimGray} Gaußsches Gesetz des Magnetismus} \\
    \rot \bm{H} &= \frac{\partial \bm{D}}{\partial t} + \bm{j} && \text{\color{DimGray} Durchflutungsgesetz} \\
    \div \bm{D} &= \varrho && \text{\color{DimGray} Gaußsches Gesetz}
  \end{align}
\end{theorem*}

\begin{notice*}[Trick]
  \begin{gather*}
    \rot (\rot \bm{E}) = - \rot \dot{\bm{B}} = \varepsilon_0 \mu_0 \ddot{\bm{E}}
  \end{gather*}
  %
  Benutze die Identität $\rot \rot = \grad \div - \div \grad = \Delta$
  %
  \begin{gather*}
    \Delta \bm{E} = \varepsilon_0 \mu_0 \frac{\partial^2 \bm{E}}{\partial t^2}
  \end{gather*}
\end{notice*}

Verallgemeinerung: Allgemeine Wellengleichung.
%
\begin{align}
  \boxed{\Delta \bm{\xi} = \frac{1}{c^2} \ddot{\bm{\xi}}} \quad \text{mit } c = \frac{1}{\sqrt{\varepsilon_0 \mu_0}}
\end{align}
%
wobei $c$ die Lichtgeschwindigkeit im Vakuum ist.

Die Gleichung gilt komponentenweise (Überlagerungsprinzip), als Folge ihrer Linearität. Betrachten wir der Einfachheit halber eine linear polarisierte Welle, bei der nur noch eine Komponente des $E$-Feldes ungleich Null ist:
%
\begin{align*}
  \bm{E} = (0,E,0)
\end{align*}
%
Einsetzen liefert:
%
\begin{align}
  (\Delta \bm{E})_y = \frac{1}{c^2} \ddot{E}_y \label{eq:1.6}
\end{align}
%
$E_y$ hängt nur von der Ausbreitungsrichtung der Welle \eqref{eq:1.6} ab (hier $z$). Somit vereinfacht sich Gleichung \eqref{eq:1.6} zu:
%
\begin{align}
  \frac{\partial^2 E_y}{\partial z^2} = \frac{1}{c^2} \frac{\partial^2 E_y}{\partial t^2} \label{eq:1.7}
\end{align}
%
\begin{figure}[H]
  \centering
  \begin{pspicture}(-1,-1)(4,1.5)
    % Achsen
    \psaxes[labels=none,ticks=none]{->}(0,0)(-0.5,-0.5)(4,1.5)[\color{DimGray} $z$,-90][\color{DimGray} $y$,180]
    \psline{->}(0,0)(-1,-1)
    \uput[180](-1,-1){\color{DimGray} $x$}
    % Sinus
    \psplot[plotpoints=200,linecolor=MidnightBlue]{0}{3.7}{0.8*sin(8*x)}
  \end{pspicture}
  \caption*{Elektrisches Feld einer elektromagnetischen Welle. Lösung von \eqref{eq:1.7}.}
\end{figure}

\begin{figure}[H]
  \centering
  \begin{pspicture}(0,-1)(2,2)
    % Goldschicht
    \psframe[linestyle=none,fillstyle=hlines,hatchcolor=DarkOrange3](0,0)(2,-0.2)
    \uput[-90](1.5,-0.2){\color{DarkOrange3} Goldschicht}
    % Prisma
    \psline(0,0)(2,0)
    \psline(0,0)(2;60)
    \psline(2;60)(2,0)
    % exponentieller Abfall
    \psline[linecolor=MidnightBlue]{<->}(0,-1)(0,0)(0.8,0)
    \psplot[linecolor=MidnightBlue]{0.02}{0.7}{-1/(20*x+0.8)}
    \uput[180](0,-0.5){\color{MidnightBlue} exponentiell gedämpft}
    % Laserstrahl
    \rput(1,0){
      \psline[linecolor=DarkRed](2;135)(1;120)(0,0)(1;60)(2;45)
      \uput[190](2;135){\color{DarkRed} Laserstrahl}
    }
  \end{pspicture}
  \caption*{Glasprisma}
\end{figure}

Die allgemeine Lösung von \eqref{eq:1.7} lautet
%
\begin{align}
  E_y = {E_y}_0 \, f(z - c t) \label{eq:1.8}
\end{align}
%
Einsetzen:
%
\begin{align*}
  \frac{\partial^2 E_y}{\partial z^2} &= {E_y}_0 \, f''(z - c t) \\
  &= \frac{1}{c^2} \frac{\partial^2 E_y}{\partial t^2} = \frac{1}{c^2} {E_y}_0 \, f''(z - c t) \, c^2
\end{align*}
%
Somit sieht man, dass alle Funktionen der Form \eqref{eq:1.8} eine Lösung der Wellengleichung darstellen. Spezielle Lösung der Wellengleichung sind die harmonischen Wellen:
%
\begin{align}
  \boxed{f(z - c t) = \sin[k (z - c t)]} \quad \text{mit der Wellenzahl} \quad \boxed{k = \frac{2 \pi}{\lambda}}
\end{align}
