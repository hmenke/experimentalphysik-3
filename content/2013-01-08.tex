% Henri Menke, Michael Schmid, 2012 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

% Vorlesung am 08.01.2013

\subsection{Brechungsgesetz nach dem Fermatschen Prinzip}

\begin{figure}[H]
  \centering
  \begin{pspicture}(-1.5,-1)(1.5,1)
    \psline(-1.5,0)(1.5,0)
    \uput[90](1.5,0){\color{DimGray} $n_e$}
    \uput[-90](1.5,0){\color{DimGray} $n_t$}
    \psline[linecolor=DarkOrange3](-1,1)(0,0)(0.5,-1)
    \psline[linestyle=dotted,dotsep=1pt](0,1)(0,-1)
    \psline[linestyle=dotted,dotsep=1pt](-1,1)(-1,-1)
    \psline[linestyle=dotted,dotsep=1pt](0.5,1)(0.5,-1)
    \psdots(-1,1)(0.5,-1)
    \uput[135](-1,1){\color{DimGray} $Q$}
    \uput[-45](0.5,-1){\color{DimGray} $P$}
    \uput[-90](-0.5,0){\color{DimGray} $x$}
    \psline{<->}(-1.2,0)(-1.2,1)
    \uput[180](-1.2,0.5){\color{DimGray} $h_Q$}
    \psline{<->}(-1.2,0)(-1.2,-1)
    \uput[180](-1.2,-0.5){\color{DimGray} $h_P$}
    \psline{<->}(-1,-1.2)(0.5,-1.2)
    \uput[-90](-0.25,-1.2){\color{DimGray} $A$}
    \psarc[linecolor=DarkOrange3]{->}(0,0){0.8}{90}{135}
    \uput{0.4}[112.5](0,0){\color{DarkOrange3} $\theta_e$}
    \psarc[linecolor=DarkOrange3]{->}(0,0){0.8}{-90}{-64}
    \uput{0.4}[-75](0,0){\color{DarkOrange3} $\theta_t$}
  \end{pspicture}
\end{figure}

Für die optische Weglänge gilt:
\[
  W = n_e \sqrt{h_Q^2 + x^2} + n_t \sqrt{h_P^2 + (A-x)^2} .
\]
Verwenden wir das Fermatsche Prinzip folgt:
%
\begin{align*}
  \frac{\mathrm{d}W}{\mathrm{d}x} &\stackrel{!}{=} 0 \\
  n_e \frac{x}{\sqrt{n_Q^2 + x^2}} &= n_t \frac{A-x}{\sqrt{h_P^2 + (A-x)^2}} .
\end{align*}
%
Durch Verwendung der trigonometrischen Eigenschaften folgt:
\[
  \boxed{n_e \sin(\theta_e) = n_t \sin(\theta_t)} 
\]

\subsection{Reflexionsgesetz in vektorieller Form}

\begin{figure}[H]
  \centering
  \begin{pspicture}(-1.5,0)(1.5,1)
    \psline(-1,0)(1,0)
    \psline(0,0)(0,1)
    \uput[0](0,1){\color{DimGray} $\bm{U}_n$}
    
    \psline[linecolor=DarkOrange3]{->}(0,0)(1.5;30)
    \psline[linecolor=DarkOrange3]{<-}(0,0)(1.5;150)
    
    \psarc[linecolor=MidnightBlue]{->}(0,0){0.7}{90}{150}
    \psarc[linecolor=MidnightBlue]{<-}(0,0){0.7}{30}{90}
    
    \uput{0.3}[120](0,0){\color{MidnightBlue} $\theta_e$}
    \uput{0.3}[60](0,0){\color{MidnightBlue} $\theta_r$}
    
    \uput[30](1.5;30){\color{DarkOrange3} $\bm{s}_e$}
    \uput[150](1.5;150){\color{DarkOrange3} $\bm{s}_r$}
  \end{pspicture}
\end{figure}

\[
  \bm{s}_r = \bm{s}_e + 2 \bm{u}_n \cos(\theta_e) = \bm{s}_e - 2 (\bm{s}_s\bm{u}_n) \bm{u}_n
\]

\begin{example*} ~
%
\begin{figure}[H]
  \centering
  \begin{pspicture}(-4,-1)(0,1)
    \psline(0,0)(1.5;135)
    \psline(0,0)(1.5;-135)
    \psarc(0,0){0.6}{135}{-135}
    \uput{0.3}[180](0,0){\color{DimGray} $\alpha$}
    \psline[linecolor=DarkOrange3]{->}(1;-135)(1;135)
    \psline[linecolor=DarkOrange3]{->}(-4,0.707)(1;-135)
    \psline[linecolor=DarkOrange3]{<-}(-4,-0.707)(1;135)
    \psarc(-2.3535,0){0.5}{25}{155}
    \uput[90](-2.3535,0){\color{DimGray} $2 \gamma$}
  \end{pspicture}
\end{figure}
%
\begin{notice*}[Anwendung]
  \begin{enum-roman}
  \item Katzenaugen, $3$ Spiegel in Konfiguration, Quadratecke ($\gamma=90^\circ$)  
  \item Ablenkung: $180^\circ$ aus allen Richtungen
  \end{enum-roman}
\end{notice*}
\end{example*}

Auch das Snelliussche Brechungsgesetz kann vektoriell formuliert werden:
%
\begin{figure}[H]
  \centering
  \begin{pspicture}(-1.5,-1)(1.5,1)
    \psline(-1.5,0)(1.5,0)
    \psline[linestyle=dotted,dotsep=1pt](0,1)(0,-1)
    \uput[90](1.5,0){\color{DimGray} $n_e$}
    \uput[-90](1.5,0){\color{DimGray} $n_t$}
    
    \psline[linecolor=DarkOrange3](-1,1)(0,0)(0.5,-1)
    \uput[180](-1,1){\color{DarkOrange3} $\bm{s}_e$}
    \uput[0](0.5,-1){\color{DarkOrange3} $\bm{s}_r$}
    
    \psline[linecolor=MidnightBlue]{->}(0,0)(0,-1)
    \uput[180](0,-1){\color{MidnightBlue} $\bm{U}_n$}
    
    \psarc[linecolor=DarkOrange3]{->}(0,0){0.8}{90}{135}
    \uput{0.4}[112.5](0,0){\color{DarkOrange3} $\theta_e$}
    \psarc[linecolor=DarkOrange3]{->}(0,0){0.8}{-90}{-64}
    \uput{0.4}[-75](0,0){\color{DarkOrange3} $\theta_t$}
  \end{pspicture}
\end{figure}
%
\begin{align*}
  n_t \bm{s}_t &= n_e \bm{s}_e + (n_t \cos(\theta_t) - n_e \cos(\theta_e)) \bm{u}_n
\intertext{Mit $\cos(\theta_e) = \bm{u}_n \bm{s}_e$ folgt:}
  n_t \cos(\theta_e) &= \sqrt{n_t^2-n_e^2+ n_e^2 \cos^2(\theta_e)} \bm{u}_n
\end{align*}
%

\begin{example*}
Fata Morgana:
%
\begin{figure}[H]
  \centering
  \begin{pspicture}(-1.5,-0.6)(1.5,1)
    \psline(-1.5,0)(1.5,0)
    \rput(-1,0){
      \psline(0,0.3)(0,0.6)
      \rput(0,0.3){
        \psline(0,0)(0.3;-70)
        \psline(0,0)(0.3;-110)
      }
      \rput(0,0.5){
        \psline(0,0)(0.25;-60)
        \psline(0,0)(0.25;-120)
      }
      \psdot(0,0.6)
    }
    \psframe(1,0)(1.3,0.6)
    \psframe(1,0)(1.3,-0.6)
    \pscurve[linecolor=DarkOrange3](-0.8,0.5)(0,0.2)(0.8,0.5)
    \psline[linestyle=dotted,dotsep=1pt,linecolor=DarkOrange3](-0.8,0.5)(0.8,-0.5)
    \uput[-90](-0.5,0){\color{DimGray} heiß}
    
    \rput(-2,0){
      \psaxes[labels=none,ticks=none]{->}(0,0)(0,0)(-0.7,1)[\color{DimGray} $n(z)$,180][\color{DimGray} $z$,0]
      \psbcurve(-0.6,1)(-0.5,0.5)(-0.3,0)
    }
  \end{pspicture}
\end{figure}
%
Glucoselösung:
%
\begin{figure}[H]
  \centering
  \begin{pspicture}(-1.5,-0.6)(1.5,1)
    \psframe[fillstyle=gradient,gradbegin=white,gradend=MidnightBlue,gradmidpoint=0](-1,0)(1,1)
    \psline[linecolor=DarkOrange3](1,0.3)(2,-0.6)
    \psline[linecolor=DarkOrange3](1,0.5)(2,-0.4)
    \pscurve[linecolor=DarkOrange3](1,0.3)(0,0.4)(-0.8,0)
    \pscurve[linecolor=DarkOrange3](1,0.5)(0,0.8)(-1,0.2)
    \uput[90](0,1){\color{DimGray} reines Wasser}
    \uput[-90](0,0){\color{DimGray} konz. Glucoselösung}
    
    \rput(-2,0){
      \psaxes[labels=none,ticks=none]{->}(0,0)(0,0)(-0.7,1)[\color{DimGray} $n(z)$,180][\color{DimGray} $z$,0]
      \psbcurve(-0.2,1)(-0.3,0.5)(-0.5,0)
    }
  \end{pspicture}
\end{figure}
%
~
\end{example*}

\subsection{Strahlenablenkung durch ein Prisma}
%
\begin{figure}[H]
  \centering
  \begin{pspicture}(0,0)(2,2)
    % Prisma
    \psline(0,0)(2,0)(2;60)(0,0)
    % Laserstrahl
    \psline[linecolor=MidnightBlue]{->}(-1,0)(1;60)
    \rput(1;60){
      \psline[linecolor=MidnightBlue,linestyle=dotted,dotsep=1pt](0,0)(2;30)
      \psline(0,0)(1;150)
      \psarc[linecolor=MidnightBlue]{->}(0,0){0.8}{150}{210}
      \uput{0.3}[180](0,0){\color{MidnightBlue} $\theta_{e1}$}
      \psline[linecolor=MidnightBlue](0,0)(1;0)
      \rput(1;0){
        \psline[linecolor=MidnightBlue,linestyle=dotted,dotsep=1pt](0,0)(-2;-30)
        \psline(0,0)(1;30)
        \psarc[linecolor=MidnightBlue]{<-}(0,0){0.8}{-30}{30}
        \uput{0.3}[0](0,0){\color{MidnightBlue} $\theta_{e2}$}
        \psline[linecolor=MidnightBlue]{->}(0,0)(2;-30)
      }
    }
    \rput(1,1.18){
      \psarc[linecolor=DarkOrange3]{<-}(0,0){0.8}{-30}{30}
      \uput{0.3}[0](0,0){\color{DarkOrange3} $\delta$}
    }
    \rput(2;60){
      \psarc[linecolor=DimGray]{-}(0,0){0.4}{-120}{-60}
      \uput{0.2}[-90](0,0){\color{DimGray}\small $\alpha$}
    }
    \uput[90](1,0){\color{DimGray} $n$}
  \end{pspicture}
\end{figure}
%
Für den Ablenkwinkel $\delta$ gilt:
\[
  \delta = \theta_{e1} - \alpha + \arccos\left(\sin(\alpha) \sqrt{n^2-\sin^2(\theta_{e1})} - \sin(\theta_{e1})\cos(\alpha) \right)
\]
Bei symmetrischer Durchstrahlung gilt:
%
\begin{figure}[H]
  \centering
  \begin{pspicture}(0,0)(2,2)
    % Prisma
    \psline(0,0)(2,0)(2;60)(0,0)
    % Laserstrahl
    \psline[linecolor=MidnightBlue]{->}(-1,0)(1;60)
    \rput(1;60){
      \psline(0,0)(1;150)
      \psarc[linecolor=MidnightBlue]{->}(0,0){0.8}{150}{210}
      \uput{0.3}[180](0,0){\color{MidnightBlue} $\theta_{e1}$}
      \psline[linecolor=MidnightBlue](0,0)(1;0)
      \rput(1;0){
        \psline(0,0)(1;30)
        \psarc[linecolor=MidnightBlue]{<-}(0,0){0.8}{-30}{30}
        \uput{0.3}[0](0,0){\color{MidnightBlue} $\theta_{t2}$}
        \psline[linecolor=MidnightBlue]{->}(0,0)(2;-30)
      }
    }
  \end{pspicture}
\end{figure}
%
$\theta_{e1} = \theta_{t2} \quad , \quad \delta \text{ minimal}$


Anwendung:
\begin{enum-roman}
  \item spektroskopische Nutzung durch ein Prismenspektralapparat (Kenntnis von $n(\lambda)$ und $\alpha$ bzw. $\delta$ zur Bestimmung von $\lambda$)
  \item Messung von Brechungsindizes
  %
  \begin{figure}[H]
    \centering
    \begin{pspicture}(0,0)(1,1)
      % Prisma
      \psline(0,0)(1,0)(1;60)(0,0)
      \rput{0}(0.6,0.8){
        \pscurve[linecolor=DimGray](0,0)(0.7,-0.1)(0.7,0.1)(1.4,0)
        \uput[0]{0}(1.4,0){\color{DimGray} Gefäß}
      }
      \rput{0}(0.6,0.2){
        \pscurve[linecolor=DimGray](0,0)(0.6,0.1)(0.6,-0.1)(1.2,0)
        \uput[0]{0}(1.2,0){\color{DimGray} unbekannte Flüssigkeit $n(\lambda)$}
      }
    \end{pspicture}
  \end{figure}
  %
  Abbé-Refraktometer
  \item Dispersionskompensation bei Femtosekundenlaser durch Nutzung von $n(\lambda)$
  %
  \begin{figure}[H]
    \centering
    \begin{pspicture}(0,0)(5,1)
      \psline[linecolor=DarkOrange3](0,0)(1,0)(2,0.55)(3,0.55)(4,0)(5,0)
      \psline[linecolor=DarkOrange3](0,0)(1,0)(2,0.45)(3,0.45)(4,0)(5,0)
      \rput{180}(1,0){\rput(-0.25,-0.2165){\pspolygon[fillstyle=solid](0,0)(0.5,0)(0.5;60)(0,0)}}
      \rput(2,0.5){\rput(-0.25,-0.2165){\pspolygon[fillstyle=solid](0,0)(0.5,0)(0.5;60)(0,0)}}
      \rput(3,0.5){\rput(-0.25,-0.2165){\pspolygon[fillstyle=solid](0,0)(0.5,0)(0.5;60)(0,0)}}
      \rput{180}(4,0){\rput(-0.25,-0.2165){\pspolygon[fillstyle=solid](0,0)(0.5,0)(0.5;60)(0,0)}}
      \rput(0,0.1){
        \psplot[plotpoints=200,linecolor=MidnightBlue]{-0.4}{0.4}{0.4*2.7182818284590451^(-(x/0.2)^2)}
        \psline{->}(-0.2,0.5)(0.2,0.5)
      }
      \rput(5,0.1){
        \psplot[plotpoints=200,linecolor=MidnightBlue]{-0.4}{0.4}{0.4*2.7182818284590451^(-(x/0.1)^2)}
        \psline{->}(-0.2,0.5)(0.2,0.5)
      }
      \uput[-120](0,0){\color{MidnightBlue} einfallender Impuls}
      \uput[-60](5,0){\color{MidnightBlue} kürzerer Impuls als vorher}
    \end{pspicture}
  \end{figure}
  %
  normale Dispersion (in Gläsern wird blau stets stärker als rot gebrochen)
  %
  \begin{figure}[H]
    \centering
    \begin{pspicture}(-0.3,-0.3)(3,2)
      \psaxes[labels=none,ticks=none]{->}(0,0)(-0.3,-0.3)(3,2)[\color{DimGray} $\lambda$,0][{\color{DimGray} $n(\lambda)$},0]
      \psplot[linecolor=MidnightBlue,plotpoints=200,yMaxValue=1.8]{0.01}{3}{1/x^2+0.5}
      \psxTick[labelFontSize=\color{blue}](1){400 \; \mathrm{nm}}
      \psxTick[labelFontSize=\color{red}](2.5){800 \; \mathrm{nm}}
    \end{pspicture}
  \end{figure}
\end{enum-roman}

\subsection{Optische Abbildungen}
%
%           HIer muss ein Bild rein !!!!!!!!!!!!!!
%
Wichtig für die mathematisch einfache (linearisierte) Beschreibung von Strahlengängen sind kleine Winkel:
\[
  \sin(\alpha) \approx \tan(\alpha) \approx \alpha  .
\]
Dann lautet das Brechungsgesetz:
\[
  n_e \theta_e = n_t \theta_t .
\]

\begin{notice*}[Wichtige Taylorentwicklungen:]
\begin{align*}
  \sin(\alpha) &= \alpha - \frac{\alpha^3}{6} + \mathcal{O}(\alpha^4) \\
  \cos(\alpha) &= 1- \frac{\alpha^2}{2} + \mathcal{O}(\alpha^3)
\end{align*}
\end{notice*}

Für die paraxiale Optik, bzw. paraxiale Näherung gilt:
\begin{enum-roman}
  \item Strahlhöhe $h \ll$ Linsendurchmesser $D$
  \item $\alpha \to 0$ möglichst kleine Einfallswinkel 
\end{enum-roman}

\subsection{Reelle und virtuelle Abbildungen}

%
%              HIer muss ein Bild rein !!!!!!!!!!!!!!!!!!1
%

Im Gegensatz zum virtuellen Bild lässt sich ein reelles Bild mit einem Schirm auffangen. Beim reellen Bild befindet sich das abbildende Instrument nicht zwischen Bildpunkt $P$ und Beobachter, im Gegensatz zum virtuelle Bild.

\minisec{Abbildung an einem Kugelspiegel}
%
%            HIer muss ein Bild rein !!!!!!!!!!1
%
Es gilt:
\[
  \theta = \beta -\alpha = \alpha - \gamma .
\]
Für die paraxiale Optik (Vernachlässigung von $d$) gilt weiter:
%
\begin{align*}
  \tan(\gamma) &\approx \gamma = \frac{h}{g} \\
  \tan(\alpha) &\approx \alpha = \frac{h}{r} \\
  \tan(\beta) &\approx \beta = \frac{h}{b} 
\end{align*}
%
Somit erhält man die \acct{Abbildungsgleichung eines sphärischen Spiegel}:
\[
  \boxed{\frac{1}{g} + \frac{1}{b} = \frac{2}{r} = \frac{1}{f}}
\]
Für die Brennweite $f$ gilt hierbei:
\[
  f= \frac{r}{2} .
\]
%
%                 HIer muss ein Bild rein !!!!!!!!!!!!1
%

Benutzt man Strahlen, die Achsenfern auf den Spiegel treffen, so ergeben sich Abbildungsfehler, in diesen Fall sphärische-Aberation:
\[
  F \neq F' .
\]
Ein Parabolspiegel (Paraboloid) erfüllt das Fermatsche Prinzip und führt beim Einfall parallel zur optischen Achse zur Fokussierung in nur einem Brennpunkt $F$, unabhängig von der Höhe. 
